[![pipeline status](https://gitlab.inria.fr/nix-tutorial/presentation-slides/badges/master/pipeline.svg)](https://gitlab.inria.fr/nix-tutorial/presentation-slides/pipelines)
[![latest pdf](https://img.shields.io/badge/latest%20pdf-online-brightgreen)](https://gitlab.inria.fr/nix-tutorial/presentation-slides/-/jobs/artifacts/master/raw/slides.pdf\?job\=slides)

Repository containing the slides introducing the Nix tutorial about reproducible experiments with Nix.

To build the presentation:

```
nix-build default.nix
```

or

```
nix-shell default.nix --command "make"
```
