{ pkgs ? import
  ( fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {}
}:

with pkgs;
let
  texlive = pkgs.texlive.combined.scheme-full;
in
  stdenv.mkDerivation rec {
    name = "slides";
    src = pkgs.lib.sourceByRegex ./. [
      "^slides.tex"
      "^Makefile"
      "^figures"
      "^figures/.*\.jpg"
      "^figures/.*\.pdf"
      "^figures/.*\.png"
    ];
    buildInputs = [ texlive gnumake ];
    buildPhase = "make";

    installPhase = ''
      mkdir $out
      cp slides.pdf $out
    '';
  }
